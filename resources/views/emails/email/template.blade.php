@component('mail::message')
# {{ $mail['title'] }}

Search Google or type a URL in the button below

@component('mail::button', ['url' => $mail['url']])
Click Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
