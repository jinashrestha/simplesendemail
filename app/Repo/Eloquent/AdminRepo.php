<?php

namespace App\Repo\Eloquent;

use Illuminate\Http\Request;
use App\Repo\RepoInterface\AdminInterface;
use App\Models\Admin;

// class AdminRepo extends BaseRepo implements AdminInterface
// {
//     public function __construct(Admin $model)
//     {
//         parent::__construct($model);
//     }
// }

class AdminRepo implements AdminInterface
{
    public function send()
    {
     
    }

    public function index()
    {
        $data = Admin::all();
        return response()->json($data);
    }

    public function indexAll()
    {
        $data = Admin::all();
        return $data;
    }

    public function show($id)
    {
        $data = Admin::find($id);
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $data = Admin::insert($request->all());
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $data = Admin::findorFail($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function delete($id)
    {
        $data = Admin::find($id);
        $data->delete();
        return response()->json('Removed Successfully');
    }
}