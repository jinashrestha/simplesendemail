<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repo\RepoInterface\BaseInterface;
use App\Repo\RepoInterface\AdminInterface;

use App\Repo\Eloquent\BaseRepo;
use App\Repo\Eloquent\AdminRepo;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseInterface::class, BaseRepo::class);
        $this->app->bind(AdminInterface::class, AdminRepo::class);

    }
}
