<?php

namespace App\Jobs;


use App\Models\Admin;
use App\Mail\SendEmailDemo;
use App\Mail\AddUser;
use App\Mail\DeleteUser;
use App\Mail\UpdateUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{

    use InteractsWithQueue, Queueable, SerializesModels;

    protected $email, $details;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $details)
    {
        $this->details = $details;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public $tries = 2;

    public function tags()
    {
        return ['send email'];
    }

    public function handle()
    {
       Mail::to($this->email)->send($this->details);
    }
}
