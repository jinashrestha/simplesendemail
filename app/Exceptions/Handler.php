<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {

        
        if ($exception instanceof MethodNotAllowedHttpException or $exception instanceof NotFoundHttpException) {
            // ajax 404 json feedback


            // normal 404 view page feedback
            return response()->json(
                [
                    'status'=>404,
                    'message'=>'Requested Url Not Found'
                ], 404);

        }
        if ($exception instanceof AuthorizationException) {

            return response()->json(
                [
                    'status'=>401,
                    'message'=>'Unauthorized'
                ], 401);

        }

        if ($exception instanceof ClientException) {
            return response()->json(
                [
                    'status'=>$exception->getCode(),
                    'message'=>$exception->getMessage()
                ], $exception->getCode());
        }
        return parent::render($request, $exception);
    }
}
