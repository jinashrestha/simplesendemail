<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Repo\Eloquent\AdminRepo;
use App\Repo\RepoInterface\AdminInterface;
use Validator;
use App\Jobs\SendEmail;
use App\Mail\SendEmailDemo;
use App\Mail\DeleteUser;
use App\Mail\AddUser;
use App\Mail\UpdateUser;
use App\Mail\EmailTemplate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use RedisClient\Exception\RedisException;
use RedisClient\RedisClient;

use Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    private $adminRepo;

    public function __construct(AdminInterface $adminRepo)
    {
        $this->AdminRepo = $adminRepo;
    }

    public function sendMail()
    {
        try{
            $subject="This is a test mail.";
            $name = "Test User";
            $mailable = new SendEmailDemo($subject, $name);
            dispatch((new SendEmail('jinashresthaaa@gmail.com',$mailable))->onQueue('demo'));
            return response()->json([
                'msg' => 'Email Sent. Check your inbox.' ,
                'status' => '200',
                'message' => "Successfully Email Send",
            ], 200);         
            } catch (\Exception $exception) {
            return response()->json(
                [
                    'status' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ], $exception->getCode()
            );
        }      
    }


    public function emailTemplate()
    {
        try{
        $email = 'jinashresthaaa@gmail.com';
        $mail = [
            'title' => 'Google',
            'url' => 'https://www.google.com/'
        ];
        Mail::to($email)->send(new EmailTemplate($mail));
        return response()->json([
            'msg' => 'Mail has been sent successfully' ,
            'status' => '200',
            'message' => "Successfully Email Send",
        ], 200);  
        } catch (\Exception $exception) {
        return response()->json(
            [
                'status' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ], $exception->getCode()
        );
        }  
    }

    public function index()
    {
        try{
        $cachedEntry = Redis::get('admins');
        dd($cachedEntry);
            if(isset($cachedEntry)) {
                $admin = json_decode($cachedEntry, FALSE);
                dd($admin);
                return response()->json([
                    'status_code' => 201,
                    'message' => 'Admin Details fetched from redis',
                    'data' => $admin,
                ]);
                }else{
                 $admin = $this->AdminRepo->index();
                 Redis::set('admins', $admin);
                 return response()->json([
                     'status_code' => 201,
                     'message' => 'Admin Details fetched from database',
                     'data' => $admin,
                 ]);
            }
        }
        catch (\Exception $exception) {
            return response()->json(
                [
                    'status' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ], $exception->getCode()
            );
            }
    }

    public function indexAll()
    { 
        try {
        $data = $this->AdminRepo->indexAll();
        return response()->json([
            'status' => '200',
            'message' => "List of admin :",
            'admin' => $data,
        ], 200);
        } catch (\Exception $exception) {
        return response()->json(
            [
                'status' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ], $exception->getCode()
        );
        }
    }

    public function show($id)
    {   
        try{
        $cachedAdmin = Redis::get('admin_' . $id);
        if(isset($cachedAdmin)) {
            $admin = json_decode($cachedAdmin, FALSE);
            return response()->json([
                'status_code' => 201,
                'message' => 'Admin Details fetched from redis',
                'data' => $admin,
            ]);
        }else {
            $admin = Admin::find($id);
            Redis::set('admin_' . $id, $admin);
      
            return response()->json([
                'status_code' => 201,
                'message' => 'Admin Details fetched from database',
                'data' => $admin,
            ]);
        }
        } catch (\Exception $exception) {
        return response()->json(
            [
                'status' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ], $exception->getCode()
        );
        }

    }

    public function store(Request $request)
    { 
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required | unique:admins',
            'password' => 'required' ,  
            'image' => 'required|image|max:100' ,
        ]);    
        try{
       //$data = Admin::create($request->all());
       $data = new Admin();

       $data->name = $request->name;
       $data->address = $request->address;
       $data->phone = $request->phone;
       $data->email = $request->email;
       $data->password = $request->password;
       $data->image = $request->image;
       
        if($request->hasfile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = storage_path('app/public/images/') . $filename;
            Image::make($image)->save($location);
            $data->image = $filename;
        }  
       $data->save();
        if($data){
        $redis = Redis::connection('default');
        
        Redis::del('admin_');

        $redis->SET('admin_', json_encode([
            'name' =>  $request->name ,
            'address' =>  $request->address ,
            'phone' =>  $request->phone ,
            'email' =>  $request->email ,
            'password' =>  $request->password ,
            'image' => $request->image ,
        ]));
        $subject="User Added";
        $admins = $this->AdminRepo->indexAll();

        foreach($admins as $admin){
            $name = $admin['name'];  
            $email = $admin['email'];
             
            $mailable = new AddUser($subject, $name);   
            dispatch((new SendEmail($email, $mailable))->onQueue('user-added'));
        }
            return response()->json([
                'msg' => 'Email Sent. Check your inbox.' ,
                'status' => '200',
                'message' => "Successfully created",
            ], 200);
        }
            } catch (\Exception $exception) {
            return response()->json(
                [
                    
                    'status' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ], $exception->getCode()
            );
        }
    }

    public function update($id, Request $request)  
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'sometimes',
            'password' => 'sometimes' ,
            'image' => 'sometimes' ,
        ]);
        try{
  // $update = Admin::findOrFail($id)->update($request->all());
        
      $update = Admin::find($id);
      
      $update->name = $request->input('name');
      $update->address = $request->input('address');
      $update->phone = $request->input('phone');
     $update->email = $request->input('email');
     $update->password = $request->input('password');
      $update->image = $request->input('image');


    if ($request->hasfile('image')) {
        Storage::disk('public')->delete("images/$update->image");

        $image = $request->file('image');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = storage_path('app/public/images/') . $filename;

        Image::make($image)->save($location);

        $update->image = $filename;
        $update->save();
        
    }
        if($update) {
            Redis::del('admin_' . $id);

            $admin = Admin::find($id);
            Redis::set('admin_' . $id, $admin);

            $subject="User Updated";
            $admins = $this->AdminRepo->indexAll();
            
            foreach($admins as $admin){
                $name = $admin['name'];
                $email = $admin['email'];
                
            $mailable = new UpdateUser($subject, $name);   
            dispatch((new SendEmail($email, $mailable))->onQueue('user-updated'));
            }
            return response()->json([
                    'msg' => 'Email Sent. Check your inbox.' ,
                    'status' => '200',
                    'message' => "Successfully Updated",
                    ], 200); 
            }
        }catch (\Exception $exception) {
            return response()->json(
                [
                    
                    'status' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ], $exception->getCode()
            );
        }

    }

    public function delete($id)
    {
        try{
        Admin::findOrFail($id)->delete();
        Redis::del('admin_' . $id);

        $subject="User Deleted";
        $admins = $this->AdminRepo->indexAll();
           
            foreach($admins as $admin){
                 $name = $admin['name'];
                 $email = $admin['email'];

                 $mailable = new DeleteUser($subject, $name);   
                 dispatch((new SendEmail($email, $mailable))->onQueue('user-deleted'));
            }
            return response()->json([
                'msg' => 'Email Sent. Check your inbox.' ,
                'status' => '200',
                'message' => "Removed Successfully",
            ], 200);
        }catch (\Exception $exception) {
            return response()->json(
                [
                    'status' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ], $exception->getCode()
            );
        }
      
    }
}

