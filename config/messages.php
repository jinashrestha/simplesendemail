<?php

return [
    "code_type" => [
        "success" => 1048576,
        "error" => 2097152,
    ],
    // "resource_code" => [

    //     "channel" => 1024,
    //     "channel_category" => 2048,
    //     "movie" => 3072,
    //     "package_subscription" => 4096,
    //     "channel_subscription" => 5120,
    //     "movie_subscription" => 6144,
    //     "wallet" => 7168,
    //     "nimble" => 8192,
    //     "log" => 9216,
    //     "reseller_domain" => 10240,
    //     "privacy_policy" => 11264,
    //     "reseller_external_auth" => 12288

    // ],
    "status_code" => [
        "200" => [

            "update" => "%s updated successfully.",
            "delete" => "%s deleted successfully.",
            "get" => "%s data fetched."
            ],

        "204" => [
            "message" => "%s Not Found."
        ],
        "201" =>[
            "create" => "%s created successfully.",
        ],

        "500" => [
           "message" => "Something went wrong. Please try again later."
        ],
        "404" => [
            "message" => "%s Not Found."
        ],



    ],
    "error_messages" => [
        "package_expiry" =>  env('PACKAGE_EXPIRY_MESSAGE', "Your primary package has been expired. Please contact your service provider for further information."),
    ]
];
