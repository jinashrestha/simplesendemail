<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'mail'], function () use ($router) {
    $router->get('/', [
        'uses' => 'AdminController@sendMail']);

});

$router->group(['prefix' => 'demo-mail'], function () use ($router) {
    $router->get('/', [
        'uses' => 'AdminController@emailTemplate']);

});

$router->group(['prefix' => 'admin'], function () use ($router) {
    $router->get('/', [
        'uses' => 'AdminController@index', 'as' => 'list.admin.details']);

    $router->get('/{id}', [
        'uses' => 'AdminController@show', 'as' => 'admin.details.detail']);

    $router->post('/', [
        'uses' => 'AdminController@store', 'as' => 'add.admin.details']);

    $router->put('/{id}', [
        'uses' => 'AdminController@update', 'as' => 'update.admin.details']);

    $router->delete('/{id}', [
        'uses' => 'AdminController@delete', 'as' => 'remove.admin.details']);

});
